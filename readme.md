# Time registration server

## Requirements
Make sure Golang 1.13 is installed  
Make sure you have Docker 2.1.0 or later installed

## Running

- Navigate to the cloned source code folder with command prompt 
</br>
- type 'go build'
</br>
- run the executable file

The data of athletes is taken from a csv located in /testdata folder.
Athletes are read from athletes.csv and all the finished athletes information(with the finish time) are put into results.csv

## Building a docker image
- type 'docker build -t timing-app .'
</br>
- Run the docker image by typing
</br>
'docker run -it -p 3001:3000 timing-app'

## Testing with dummydata

- Go to websocket.in/test-online

- Enter a custom websocket url: 
ws://localhost:3000/api/ws</br>
Make sure the running port number is correct
- Click Connect

This will be showing the receiving data from the dummydata client.  When executing the dummydata client, you should see the correct data coming into this page.

### Download the dummydata client app repo
https://gitlab.com/ViljarSare/mooncascadedummy

Follow the instructions given there.



