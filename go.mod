module sportstiming

go 1.13

require (
	github.com/go-delve/delve v1.3.2
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.1
	gitlab.com/ViljarSare/mooncascadetiming v0.0.0-20191205123753-b6a33f0b4715
)
