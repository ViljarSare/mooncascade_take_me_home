package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

//websocketEP initializes websocket connection
func websocketEP(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}
	clients[conn] = true

}

//writer sends the athletes data to all the UI clients.
func writer() {
	for msg := range responseData {
		for client := range clients {
			err := client.WriteJSON(msg)
			fmt.Println(msg)
			if err != nil {
				delete(clients, client)
			}
		}
	}
}
