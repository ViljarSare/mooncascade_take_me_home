package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"sportstiming/csv"
	"sportstiming/models"
)

//TimingPoint handles incoming http POST requests with the athletes time and chipID.
//It returns the athletes full data to UI and in case of going through finish, all the data is written to CSV as well.
func TimingPoint(c map[int]models.Response, ch chan models.Response) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		contestant := models.Timing{}
		err := json.NewDecoder(r.Body).Decode(&contestant)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusBadRequest)

		}
		switch contestant.TimingPoint {
		case "corridor":
			corridor(contestant, c, ch)
		case "finish":
			finish(contestant, c, ch)

		}
	}
}

func corridor(timing models.Timing, c map[int]models.Response, ch chan models.Response) {
	athlete := c[timing.ChipID]
	athlete.FinishTime = timing.Time
	athlete.TimingPoint = timing.TimingPoint
	ch <- athlete
}

func finish(timing models.Timing, c map[int]models.Response, ch chan models.Response) {
	athlete := c[timing.ChipID]
	athlete.FinishTime = timing.Time
	athlete.TimingPoint = timing.TimingPoint
	ch <- athlete
	csv.WriteCSV(athlete)
}
