package main

import (
	"log"
	"net/http"
	"sportstiming/csv"
	"sportstiming/handlers"
	"sportstiming/models"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

//timingPointData takes the data from timing point(corridor or finish)
var timingPointData = make(chan models.Timing)

//responseData send the data to the UI(athlete chipId, forename, etc..)
var responseData = make(chan models.Response)

//clients maps all the connected UI clients who need the table of athletes completing the race
var clients = map[*websocket.Conn]bool{}

func main() {

	contestant := csv.ReadCSV()
	r := mux.NewRouter()
	r.HandleFunc("/api/timing/", handlers.TimingPoint(contestant, responseData))
	r.HandleFunc("/api/ws", websocketEP)
	go writer()
	log.Fatal(http.ListenAndServe(":3000", r))

}
