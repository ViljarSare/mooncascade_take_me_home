FROM golang:latest
RUN mkdir /timing
ADD . /timing
WORKDIR /timing
RUN go build -o main .
CMD ["/timing/main"]