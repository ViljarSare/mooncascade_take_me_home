package csv

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"sportstiming/models"
	"strconv"
)

//ReadCSV takes all the athletes from the csv and puts them in a map so when a query comes in, the athlete can be found quickly and returned to UI
func ReadCSV() map[int]models.Response {
	athletes := make(map[int]models.Response)

	csvFile, err := os.Open("testdata/athletes.csv")
	if err != nil {
		panic(err)
	}
	r := csv.NewReader(csvFile)

	for {
		line, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		} else {
			id, err := strconv.Atoi(line[0])
			if err != nil {
				log.Println(err)
			}
			start, err := strconv.Atoi(line[3])
			if err != nil {
				log.Println(err)
			}

			a := models.Response{
				ChipID:      id,
				Forename:    line[1],
				Surname:     line[2],
				StartNumber: start,
				FinishTime:  line[4],
			}
			athletes[a.ChipID] = a
		}
	}
	return athletes
}

//WriteCSV writes all the athletes who have finished into a new CSV with their finishtime.
func WriteCSV(m models.Response) {
	file, err := os.OpenFile("testdata/results.csv", os.O_APPEND, 0222)
	if err != nil {
		log.Println(err)
	}
	defer file.Close()

	chip := strconv.Itoa(m.ChipID)
	startNumber := strconv.Itoa(m.StartNumber)
	s := []string{chip, m.Forename, m.Surname, startNumber, m.FinishTime}
	fmt.Println(s)
	w := csv.NewWriter(file)

	err = w.Write(s)
	if err != nil {
		log.Println(err)
	}
	w.Flush()
}
