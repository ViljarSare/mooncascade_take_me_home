package models

type Timing struct {
	ChipID      int    `json:"chip_id,omitempty"`
	TimingPoint string `json:"timing_point,omitempty"`
	Time        string `json:"time,omitempty"`
}

type Response struct {
	ChipID      int    `json:"chipID"`
	Forename    string `json:"forename"`
	Surname     string `json:"surname"`
	StartNumber int    `json:"startNumber"`
	FinishTime  string `json:"finishTime"`
	TimingPoint string `json:"timing_point,omitempty"`
}
